terraform {
  backend "s3" {
    bucket = "cicd-peri1234"
    key    = "state"
    region = "us-east-1"
  }
}
