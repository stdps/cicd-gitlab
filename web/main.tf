#ec2

resource "aws_instance" "server" {
  ami             = "ami-0c101f26f147fa7fd"
  instance_type   = "t2.micro"
  subnet_id       = var.sn
  security_groups = [var.sg]

  tags = {
    "Name" = "myserver"
  }
}
