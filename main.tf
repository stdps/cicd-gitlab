terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.42.0"
    }
  }
}

module "vpc" {
  source = "./vpc"
}

module "ec2" {
  source = "./web"
  sn     = module.vpc.pb_sn
  sg     = module.vpc.sg
}
